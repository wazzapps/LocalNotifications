package com.wazzapps.notifications;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;

import androidx.core.app.NotificationManagerCompat;

import com.unity3d.player.UnityPlayer;

import java.util.List;

public class UnityNotificationManager extends BroadcastReceiver {
    private final static String TAG =  "UnityNotification";
    private final static String unityClassDefault = "com.unity3d.player.UnityPlayerActivity";

    public static void SendNotification(int id, long delayMs, String title, String message, int smallBitmap, String bigBitmap,
    		String ticker, boolean sound, boolean vibrate, String key, String unityClass) {

        Activity currentActivity = UnityPlayer.currentActivity;
        Intent intent = new Intent(currentActivity, UnityNotificationManager.class);
        intent.putExtra("ticker", ticker);
        intent.putExtra("title", title);
        intent.putExtra("message", message);
        intent.putExtra("smallBitmap", smallBitmap);
        intent.putExtra("bigBitmap", bigBitmap);
        intent.putExtra("id", id);
        intent.putExtra("sound", sound);
        intent.putExtra("vibrate", vibrate);
        intent.putExtra("activity", unityClass);
        intent.putExtra("key", key);

        // TimerService.StartService(currentActivity);
        // TimerService.AddAlarm(currentActivity, id, delayMs, intent);

       AlarmManager alarmManager = (AlarmManager)currentActivity.getSystemService(Context.ALARM_SERVICE);
       PendingIntent pendingIntent = PendingIntent.getBroadcast(currentActivity, id, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
       alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + delayMs, pendingIntent);

        Log.d(TAG, "Scheduled notification " + id + " after " + delayMs + "ms");
    }

    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Received notification");
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        String ticker = intent.getStringExtra("ticker");
        String title = intent.getStringExtra("title");
        String message = intent.getStringExtra("message");
        int smallBitmap = intent.getIntExtra("smallBitmap", 0);
        String bigBitmap = intent.getStringExtra("bigBitmap");
        String key = intent.getStringExtra("key");
        String unityClass = intent.getStringExtra("activity");
        Boolean sound = intent.getBooleanExtra("sound", false);
        Boolean vibrate = intent.getBooleanExtra("vibrate", false);
        int id = intent.getIntExtra("id", 0);

        Class<?> unityClassActivity = null;
        try {
            unityClassActivity = Class.forName(unityClass);
        } catch (ClassNotFoundException e) {
            Log.d(TAG, "Unable to find App class name " + unityClass);
            try {
                unityClassActivity = Class.forName(unityClassDefault);
            } catch (ClassNotFoundException e1) {
                Log.d(TAG, "Unable to find App class name " + unityClassDefault);
                return;
            }
        } catch (NullPointerException e) {
            Log.d(TAG, "Unable to find App class name " + unityClass + " and " + unityClassDefault);
            return;
        }

        Log.d(TAG, "Start check if class exists: " + unityClassDefault + " and " + unityClass);
        if(unityClassActivity != null)
        {
            Log.d(TAG, "Notification for class " + unityClassActivity.getName());
        } else {
            Log.d(TAG, "Unable to find App class name " + unityClass + " and " + unityClassDefault);
            return;
        }

        Intent notificationIntent = new Intent(context, unityClassActivity);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        notificationIntent.putExtra("fromNotification", true);
        notificationIntent.putExtra("notificationTime", System.currentTimeMillis()+"");
        notificationIntent.putExtra("key", key);

        PendingIntent contentIntent = PendingIntent.getActivity(context, id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        
        Notification.Builder builder = new Notification.Builder(context);
        builder.setAutoCancel(true);

        builder.setContentIntent(contentIntent)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(message);

        if(ticker != null && ticker.length() > 0) {
            builder.setTicker(ticker);
        }

        switch (smallBitmap) {
            case 0:
                builder.setSmallIcon(R.drawable.cat_i);
                break;
            case 1:
                builder.setSmallIcon(R.drawable.check_i);
                break;
            case 2:
                builder.setSmallIcon(R.drawable.chest_i);
                break;
            case 3:
                builder.setSmallIcon(R.drawable.crystal_i);
                break;
            case 4:
                builder.setSmallIcon(R.drawable.flash_i);
                break;
            case 5:
                builder.setSmallIcon(R.drawable.i_i);
                break;
            case 6:
                builder.setSmallIcon(R.drawable.lamp_i);
                break;
            case 7:
                builder.setSmallIcon(R.drawable.plane_i);
                break;
            case 8:
                builder.setSmallIcon(R.drawable.tank_i);
                break;
            case 9:
                builder.setSmallIcon(R.drawable.target_i);
                break;
            case 10:
                builder.setSmallIcon(R.drawable.train_i);
                break;
            case 11:
                builder.setSmallIcon(R.drawable.pill_i);
                break;
            default:
                builder.setSmallIcon(R.drawable.i_i);
                Log.d(TAG, "Unable to find small icon with id=" + smallBitmap);
                break;
        }

        try
        {
            byte[] bigBitmapData = Base64.decode(bigBitmap, Base64.DEFAULT);
            Bitmap decodedBigBitmap = BitmapFactory.decodeByteArray(bigBitmapData, 0, bigBitmapData.length);
            builder.setLargeIcon(decodedBigBitmap);
        } catch (Exception e)
        {
            Log.d(TAG, "Unable to load big icon: " + e.getMessage());
        }

        if(sound) {
            builder.setSound(RingtoneManager.getDefaultUri(2));
        }

        if(vibrate) {
            builder.setVibrate(new long[] {
                    300L, 300L
            });
        }

        Notification notification;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "default";
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Information from game",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(channelId);
        }

        if(Build.VERSION.SDK_INT < 16) {
            notification = builder.getNotification();
        } else {
            notification = builder.build();
        }

        Log.d(TAG, "Notify " + id);
        notificationManager.notify(id, notification);
    }

    public static void CancelNotification(int id) {
        Activity currentActivity = UnityPlayer.currentActivity;
        AlarmManager am = (AlarmManager)currentActivity.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(currentActivity, UnityNotificationManager.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(currentActivity, id, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        if(pendingIntent != null)
        {
            pendingIntent.cancel();
            Log.d(TAG, "Cancel notification " + id);
        }
        am.cancel(pendingIntent);
    }
    public static void CancelAll() {
        Log.d(TAG, "Cancel all notifications");
        for(int i = 0; i < 32; i++)
        {
            CancelNotification(i);
        }
    }

    public static void ClearAll(){
        NotificationManager notificationManager = (NotificationManager)UnityPlayer.currentActivity.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        Log.d(TAG, "Clear all notifications");
    }

    public static boolean IsDeviceNotificationEnabled()
    {
        Context context = UnityPlayer.currentActivity.getApplicationContext();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationManager manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (!manager.areNotificationsEnabled())
            {
                return false;
            }
            List<NotificationChannel> channels = manager.getNotificationChannels();
            for (NotificationChannel channel : channels)
            {
                if (channel.getImportance() == NotificationManager.IMPORTANCE_NONE)
                {
                    return false;
                }
            }
            return true;
        }
        else
        {
            return NotificationManagerCompat.from(context).areNotificationsEnabled();
        }
    }
}

