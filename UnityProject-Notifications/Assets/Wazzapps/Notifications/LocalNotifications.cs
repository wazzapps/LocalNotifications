﻿using UnityEngine;
using System;
#if UNITY_IOS && !UNITY_EDITOR
using UnityEngine.iOS;
#endif

class LocalNotification
{
	public enum AndroidSmallIcon
	{
		Cat, Done, Chest, Crystal, Flash, Info, Lamp, Plane, Tank, Target, Train, Pill
	}

#if UNITY_ANDROID && !UNITY_EDITOR
	private static string fullClassName = "com.wazzapps.notifications.UnityNotificationManager";
	private static string mainActivityClassName = "org.wazzapps.wazzappssdkunity.WazzappsUnitySDKActivity";
#endif

    public static int SendNotification(int id, long delayInSec, string title, string message, 
		AndroidSmallIcon smallIcon = AndroidSmallIcon.Done, Texture2D bigIcon = null, string key = "", bool sound = true, bool vibrate = false)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		id = id % 32;
		AndroidJavaClass pluginClass = new AndroidJavaClass(fullClassName);
		string encodedBigTexture = Convert.ToBase64String (bigIcon.EncodeToPNG());
		pluginClass.CallStatic("SendNotification", id, delayInSec * 1000L, title, message, (int)smallIcon, encodedBigTexture,
			message, sound, vibrate, key, mainActivityClassName);
		Debug.Log("Send android notification " + id);
		return id;
#elif UNITY_IOS && !UNITY_EDITOR
		return ScheduleIOSNotification(id, delayInSec, 0, title, message, sound);
#endif
        return 0;
    }

#if UNITY_IOS && !UNITY_EDITOR
	private static int ScheduleIOSNotification(int id, long delay, long timeout, string title, string message, bool sound) {
		UnityEngine.iOS.LocalNotification notification = new UnityEngine.iOS.LocalNotification();
		notification.fireDate = System.DateTime.Now.AddSeconds(delay);
		notification.alertAction = title;
		notification.alertBody = message;
		if(sound == true) {
			notification.soundName = UnityEngine.iOS.LocalNotification.defaultSoundName; 
		}
		notification.userInfo = new System.Collections.Generic.Dictionary<string,int>() {{"id", id}};
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notification);

		Debug.Log("Send ios notification " + id);
        return id;
	}
#endif

    public static void CancelAllNotifications()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass pluginClass = new AndroidJavaClass(fullClassName);
		if (pluginClass != null) 
		{
			for(int i = 0; i < 32; i++)
			{
				pluginClass.CallStatic("CancelNotification", i);
			}
		}
#elif UNITY_IOS && !UNITY_EDITOR
		UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
#endif
    }

    public static void ClearAllNotifications()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass pluginClass = new AndroidJavaClass(fullClassName);
		if (pluginClass != null) {
			pluginClass.CallStatic("ClearAll");
		}
#elif UNITY_IOS && !UNITY_EDITOR
		UnityEngine.iOS.NotificationServices.ClearLocalNotifications();
#endif
    }

    public static void CancelNotification(int id)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass pluginClass = new AndroidJavaClass(fullClassName);
		if (pluginClass != null) 
		{
			pluginClass.CallStatic("CancelNotification", id);
		}
#elif UNITY_IOS && !UNITY_EDITOR
        foreach (UnityEngine.iOS.LocalNotification notif in UnityEngine.iOS.NotificationServices.scheduledLocalNotifications) 
        { 
            if ((int)notif.userInfo["id"] == id)
            {
                UnityEngine.iOS.NotificationServices.CancelLocalNotification(notif);
            }
        }
#endif
    }

    public static bool IsLaunchedFromNotification()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
    	AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
		AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
		return intent.Call<bool> ("getBooleanExtra", "fromNotification", false);
#elif UNITY_IOS && !UNITY_EDITOR
#endif
		return false;
    }

    public static string GetNotificationKey()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
    	AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
		AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
		string value = intent.Call<string> ("getStringExtra", "key");
		if(value == null) value = "";
		return value;
#elif UNITY_IOS && !UNITY_EDITOR
		if (UnityEngine.iOS.NotificationServices.localNotifications != null && UnityEngine.iOS.NotificationServices.localNotifications.Length > 0)
		{
			var notification = UnityEngine.iOS.NotificationServices.localNotifications[0];
			if (notification.userInfo != null)
			{
				return notification.userInfo["key"].ToString();
			}
		}
#endif
		return "";
    }

    public static long GetLaunchNotificationTime()
    {
    	string value = "0";
#if UNITY_ANDROID && !UNITY_EDITOR
    	AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
		AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
		value = intent.Call<string> ("getStringExtra", "notificationTime");
#elif UNITY_IOS && !UNITY_EDITOR
#endif
		long time = 0;
		if(Int64.TryParse(value, out time)) return time;
		return 0;
    }

    public static bool IsDeviceNotificationEnabled()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
    	AndroidJavaClass pluginClass = new AndroidJavaClass(fullClassName);
		if (pluginClass != null) {
		return	pluginClass.CallStatic<bool>("IsDeviceNotificationEnabled");
		}
#elif UNITY_IOS && !UNITY_EDITOR
		return UnityEngine.iOS.NotificationServices.enabledNotificationTypes != NotificationType.None;
#endif
	    return false;
    }
}
