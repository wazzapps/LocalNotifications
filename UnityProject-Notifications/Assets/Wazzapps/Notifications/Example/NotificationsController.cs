﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationsController : MonoBehaviour
{
    private enum NotificationId
    {
        Id1
    }

    [SerializeField] private Texture2D notificationIcon;

    private void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus)
        {
            CancelAll();
        }
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus)
        {
            CancelAll();
        }
        else
        {
            Schedule();
        }
    }

    private void CancelAll()
    {
        LocalNotification.CancelAllNotifications();
    }

    private void Schedule()
    {
/*            LocalNotification.SendNotification((int) NotificationId.Id1,
                3600,
                "notification_title",
                "notification_text",
                LocalNotification.AndroidSmallIcon.Info,
                notificationIcon);*/
    }
}