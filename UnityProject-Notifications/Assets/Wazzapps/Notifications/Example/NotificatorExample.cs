﻿using UnityEngine;
using UnityEngine.UI;

public class NotificatorExample : MonoBehaviour {
	public Texture2D BigIcon;

	int _notificationId = 0;

	public void NotifyIn1Minute()
	{
		_notificationId = LocalNotification.SendNotification(0, 30, "NotificatorExample title", "NotificatorExample description", 
			LocalNotification.AndroidSmallIcon.Cat, BigIcon, "keyForNotification!");
	}
	public void CancelNotification()
	{
		LocalNotification.CancelNotification(_notificationId);
	}
	public void CancelAllNotification()
	{
		LocalNotification.CancelAllNotifications();
	}
	public void ClearNotifications()
	{
		LocalNotification.ClearAllNotifications();
	}

	void Start()
	{
		Debug.Log("IsLaunchedFromNotification: " + LocalNotification.IsLaunchedFromNotification());
		Debug.Log("GetLaunchNotificationTime: " + LocalNotification.GetLaunchNotificationTime());
		Debug.Log("NotificationKey: " + LocalNotification.GetNotificationKey());
		Debug.Log($"Device notification enable {LocalNotification.IsDeviceNotificationEnabled()}");
	}
}
