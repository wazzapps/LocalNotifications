# Local notifications #

Билблиотека позволяет работать с нотификациями (android/ios). Для установки нотификации:
```
Wazzapps.LocalNotification.SendNotification(int id, long delayInSec, string title, string message, AndroidSmallIcon smallIcon = AndroidSmallIcon.Done, Texture2D bigIcon = null, bool sound = true, bool vibrate = false);
```

Также доступны:
```
CancelAllNotifications();
CancelNotification(int id);
ClearAllNotifications();
```

Для того, чтобы передать иконку для нотификации используется параметр bigIcon. При этом необходимо передавать Texture2D, для которой включено Read/Write и отключена компрессия в Import Settings.